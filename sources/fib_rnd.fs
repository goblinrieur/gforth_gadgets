\ creates from number a coherant < 10 list of numbers
REQUIRE random.fs
HERE SEED !
rnd 
: fib ( x1 x2 -- x2 x3 )
    2dup + rot drop
;
: play ( n -- [sound] )
	case
		0 of s" mplayer 0.wav > /dev/null 2>&1 & " system endof
		1 of s" mplayer 1.wav > /dev/null 2>&1 & " system endof
		2 of s" mplayer 2.wav > /dev/null 2>&1 & " system endof
		3 of s" mplayer 3.wav > /dev/null 2>&1 & " system endof
		4 of s" mplayer 4.wav > /dev/null 2>&1 & " system endof
	    5 of s" mplayer 5.wav > /dev/null 2>&1 & " system endof
		6 of s" mplayer 6.wav > /dev/null 2>&1 & " system endof
		7 of s" mplayer 7.wav > /dev/null 2>&1 & " system endof
		8 of s" mplayer 8.wav > /dev/null 2>&1 & " system endof
		9 of s" mplayer 9.wav > /dev/null 2>&1 & " system endof
	endcase
;

: main ( a b -- loop[x] )
	cr 50 0 do
		key? if 
			key 32 = if exit then 	\ stop run loop on space key
		then
		fib dup 9 > if		\ limit to 10 samples 
			begin 
				5 6 random + dup	\ 9 - ( 5 + [0-6] ) randomly
			1 8 within until -	\ check value is possible 
		then
		dup play 250 ms		\ play next 
	loop
;
9 random 9 random main cr 0 (bye) \ even starting point is random

