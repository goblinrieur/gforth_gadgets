variable cpt
: letsget
	page 8 cpt ! 				\ start on clean page
	990000 0 do					\ loop over 90000
		I dup ." -[  " . ." =   "  xemit ."  ]---"
		cpt @ 1 - 1 < if 
			10 cpt ! cr cr		\ if line is full double carriage return
		else
			cpt @ 1 - cpt !		\ just count presented chars 
		then 
	loop cr  					\ at end make again an empty line
;
letsget cr 0 (bye) 				\ exit 0
