65535 constant maxline
\ buffer
create linebuffer maxline 2 + allot
\ filenames
2variable	filenameinput
2variable	filenameouput
\ file ids
0 value		inputfileid
0 value		outputfileid
\ close files buffers
: COLORIZE 
	\ colorize error message bold red on OSCII terminals
	27 EMIT ." [1;" base @ >R 0 <# #S #> type R> base ! ." m"  
; 
: cleaning
	inputfileid close-file throw 
	outputfileid close-file throw 
	0 (bye)
;
\ create buffers for data treatment
: initiating
	filenameinput 2@ r/o open-file throw to inputfileid 
	filenameouput 2@ r/w create-file throw to outputfileid
;
\ read filenames from cli arguments
: getparemters
	argc @ 3 <> if 
		\ manage bad parameter number
		cr cr 91 colorize	
		0 arg type ."  thisprogram.fs filename destinationfilename"
		0 colorize cr cr cr 
		1 (bye)
	else
		\ get filenames
		1 arg filenameinput 2!
		2 arg filenameouput 2!
	then
; 
\ copy file 
: copying
	begin
		\ line buffer read
		linebuffer maxline inputfileid read-line throw
	while
		\ line buffer copy
		linebuffer swap outputfileid write-line throw
	repeat
;
\ main code
: main
	getparemters
	initiating
	copying
	cleaning
;
main 
