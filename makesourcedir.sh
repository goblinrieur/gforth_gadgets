#! /usr/bin/env bash

find ${HOME}/GITLAB/dev/ -type f -name "*.fs" -exec cp  {} sources/ \; -print 2> /dev/shm/$0find.err 1> /dev/shm/$0find.log
find ${HOME}/GITLAB/dev/ -type f -name ".score" -exec cp  {} sources/ \; -print 2>> /dev/shm/$0find.err 1>> /dev/shm/$0find.log

if [ $? -gt 0 ] ; then
	exit 1
fi

exit 0 
