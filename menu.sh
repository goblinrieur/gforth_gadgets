#! /bin/bash

function debug()
{
	debug=1
	echo ${filelist[@]} 	# see full list
	echo ${filelist[@]:2:1} # see from third (count from 0) to current + X (1) elements 
	echo -e "\n"
	menu $param
}

function license()
{
	more ./LICENSE
	exit 0
}

function helps()
{
	echo -e "\n\n\e[1m--- help ---\e[0m\n\n"
	echo -e "\n\t $0 -d : debug use only"
	echo -e "\n\t $0 -c : display code of Gnu-Forth source"
	echo -e "\n\t $0 -l : display License"
	echo -e "\n"
	echo -e "\n...etc..\n"
	exit 0
}

function code()
{
	largeur=8
	displayedcount=0
	array=0
   	for i in ${filelist[@]} ; do 
   	   let "displayedcount+=1" 
   	   let "array+=1" 
	   if [ $array -gt 5 ]; then 
		   char="\n"
		   array=0
	   else
		  if [ $displayedcount -lt 1000 ]; then char=" \t" ; fi	   
		  if [ $displayedcount -lt 100 ]; then char="  \t" ; fi	   
		  if [ $displayedcount -lt 10 ]; then char="   \t" ; fi	   
	   fi
	   if [ $displayedcount -lt 2 ] ; then
	      printf "$displayedcount : $(basename -s .fs $i | cut -c 1-$largeur)"
       else
	      printf "$char$displayedcount : $(basename -s .fs $i | cut -c 1-$largeur)"
	   fi
    done
	echo -e "\n\n\e[1;31mChoose number :\e[0m "
	read -e reply 
	# alignement of reply to filelist array 
	let "reply-=1" 
	#more ${filelist[@]:$reply:1}
	if [ -f /usr/bin/pygmentize ] ; then 
		/usr/bin/pygmentize ${filelist[@]:$reply:1} | more || /usr/bin/more ${filelist[@]:$reply:1} 
	else
		/usr/bin/more ${filelist[@]:$reply:1}
	fi
}

function list()
{
	# use a loop function to make automatic update whatever I add or not 
	displayedcount=0
	for i in ${filelist[@]} ; do 
		let "displayedcount+=1" 
		echo -e "\t$displayedcount\t$i" | sed 's/sources.//g;s/\.fs$//' 
	done 
	read reply 
	# alignement of reply to filelist array 
	let "reply-=1" 
	$run ${filelist[@]:$reply:1}
}

function menu()
{
	if [ ! -z $debug ] ; then set -x ; echo "gforth : $version" ; fi
	echo -e "\n\n\e[1m\e[33m--- menu ---\e[0m\n\n"
	case "$1" in
		-h) helps ;;
		"") helps ;; 
		"--help") helps ;; 
		-l) license  ;;
		-c) code  ;;
		 *)  echo -e "\n\t bad parameter \n \tPlease use $0 -h for help\n" 
			echo -e "\n...etc..\n"
			exit 1 
		;; 
	esac
	unset filelist
	unset param
	unset run
	unset version
	if [ ! -z $debug ] ; then unset debug ; set +x ; fi
	exit 0
}

function checkforth()
{
run=$(which gforth)
version=$($run -e "version-string type bye")
if [[ $version  =~ ^[0-9].[0-9].[0-9]$ ]] ; then
	version=OK
else
	echo -e "\n\n\tFAIL on $run to get gnu forth $version\n\n"
	exit 1
fi
}

function main()
{
	# prepare for a good var management
	shopt -s nullglob		
	# set forth file list in an array 
	filelist=(sources/*fs)
	param=$1
	# check gforth is ok & runable
	checkforth
	# check if debug mode is set 
	if [ "$1" == "-d" ] ; then debug ; else 
		menu ${param}
	fi
}

##################### main script #####################

main $1
