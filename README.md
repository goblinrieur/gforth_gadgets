# container for gforth written gadgets

This is a development & training , just for fun around 

few gforth written scripts ( gadgets, toys, some more useful, )

## update 

sam. 14 sept. 2024 14:49:05 CEST

Simplified repo to group and store only all my .fs scripts & tools as an archive.

## example 

![image](./1.png)

## how to run 

`./menu.sh`

![image](./2.png)

## update forth source base

`./makesourcedir.sh`
